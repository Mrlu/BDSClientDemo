//
//  BDS.m
//  Pods
//
//  Created by Mrlu on 2018/9/14.
//

#import "BDSUtil.h"

@interface NSBundle (BDS_bundle)
+ (NSBundle *)BDS_bundle;
@end

@implementation NSBundle (BDS_bundle)
+ (NSBundle *)BDS_bundle {
    NSBundle *bundle = [self bundleForClass:[BDSUtil class]];
    NSURL *url = [bundle URLForResource:@"resource" withExtension:@"bundle"];
    return [self bundleWithURL:url];
}
@end

@implementation BDSUtil

+ (NSBundle *)res_bundle {
    return [NSBundle BDS_bundle];
}

@end



