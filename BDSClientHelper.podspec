#
# Be sure to run `pod lib lint BJHL-Websocket-iOS.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "BDSClientHelper"
  s.version          = "0.0.1"
  s.summary          = "BDSClient 百度语音合成"

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = <<-DESC
                BDSClient
                       DESC

  s.homepage         = "https://gitlab.com/ML-iOS/AppCore"
  s.license          = 'MIT'
  s.author           = { "mrlu" => "lujinhao@xdf.cn" }
  s.source           = { :git => "git@gitlab.com:ML-iOS/AppCore.git", :tag => s.version.to_s }

  s.platform     = :ios, '8.0'
  s.requires_arc = true
  s.ios.deployment_target = '8.0'
  s.source_files = 'Sources/**/*.{m,h}'
  s.xcconfig = {
      'OTHER_LDFLAGS' => '$(inherited) -ObjC',
  }
  s.public_header_files = 'Sources/BDSClientHeaders/TTS/*.h'
  s.vendored_libraries = 'Sources/BDSClientLib/libBaiduSpeechSDK.a'
  s.libraries        = 'c++', 'z.1.2.5', 'sqlite3.0', 'iconv.2.4.0'
  s.frameworks = 'Foundation', 'UIKit', 'AudioToolbox', 'CFNetwork', 'SystemConfiguration', 'AVFoundation', 'CoreTelephony'

  s.resource_bundle = {
      'resource' => 'Sources/BDSClientResource/TTS/*'
  }

end
