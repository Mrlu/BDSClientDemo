//
//  SpeechSynManager.swift
//  SuperLearn_toelf
//
//  Created by Mrlu on 28/11/2017.
//  Copyright © 2017 xdf. All rights reserved.
//

import UIKit
import AVFoundation

//import BDSClient
import BDSClientHelper

struct SpeechConfig {
    static let appid:String = "10411094"
    static let apikey:String = "GmvmOqbgp0IYGO9xkf9oZGSk"
    static let secretkey:String = "95HaSj7SGz8beGCLc70gAePLpDH8PXnl"
}

@objc protocol SpeechSynManagerProtocol {
    @objc optional func updatePronounce(state:Bool) -> Void
}

class SpeechSynManager: NSObject {

    static let share:SpeechSynManager = SpeechSynManager()

    typealias EndClosure = (_ view:SpeechSynManagerProtocol?) -> Void
    typealias StartClosure = (_ view:SpeechSynManagerProtocol?) -> Void
    override init() {
        super.init()
        configureSDK()
    }

    private var sentence:String?

    private var endClosure:EndClosure?
    private var startClosure:StartClosure?
    private weak var view:SpeechSynManagerProtocol?

    @discardableResult
    func end(closure:EndClosure?) -> Self {
        endClosure = closure
        return self
    }

    @discardableResult
    func start(closure:StartClosure?) -> Self {
        startClosure = closure
        return self
    }

    @discardableResult
    func play(sentence:String, view:SpeechSynManagerProtocol? = nil) throws -> Self {
        guard sentence.count > 0 else {
            return self
        }
        self.view?.updatePronounce?(state: false)
        self.view = view
        if BDSSpeechSynthesizer.sharedInstance().synthesizerStatus() == BDS_SYNTHESIZER_STATUS_WORKING {
            //切换单词，则取消合成/播放
            if self.sentence == sentence {
                BDSSpeechSynthesizer.sharedInstance().cancel()
            }
        }
        self.sentence = sentence
        var error:NSError?
        let ret = BDSSpeechSynthesizer.sharedInstance().speakSentence(sentence, withError: &error)
        if ret == -1, let _ = error {
            throw error!
        }
        return self
    }

    func stop() {
        BDSSpeechSynthesizer.sharedInstance().cancel()
    }

    private func configureSDK() {
        debugPrint("TTS version info: \(BDSSpeechSynthesizer.version())")
        BDSSpeechSynthesizer.setLogLevel(BDS_PUBLIC_LOG_ERROR)
        BDSSpeechSynthesizer.sharedInstance().setSynthesizerDelegate(self)
        configureOnlineTTS()
        configureOfflineTTS()
    }

    private func configureOnlineTTS () {
        BDSSpeechSynthesizer.sharedInstance().setApiKey(SpeechConfig.apikey, withSecretKey: SpeechConfig.secretkey)
        BDSSpeechSynthesizer.sharedInstance().setSynthParam(false, for: BDS_SYNTHESIZER_PARAM_ENABLE_AVSESSION_MGMT)
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        BDSSpeechSynthesizer.sharedInstance().setSynthParam(BDS_SYNTHESIZER_SPEAKER_FEMALE, for:BDS_SYNTHESIZER_PARAM_SPEAKER)
    }

    private func configureOfflineTTS() {
        let offlineEngineSpeechData = Bundle.main.path(forResource: "Chinese_And_English_Speech_Female", ofType: "dat")
        let offlineChineseAndEnglishTextData = Bundle.main.path(forResource: "Chinese_And_English_Text", ofType: "dat")
        let error = BDSSpeechSynthesizer.sharedInstance().loadOfflineEngine(offlineChineseAndEnglishTextData, speechDataPath: offlineEngineSpeechData, licenseFilePath: nil, withAppCode: SpeechConfig.appid)
        if error != nil {
            return;
        }
    }
}

extension SpeechSynManager:BDSSpeechSynthesizerDelegate {

    func synthesizerStartWorkingSentence(_ SynthesizeSentence: Int) {

    }

    func synthesizerFinishWorkingSentence(_ SynthesizeSentence: Int) {

    }

    func synthesizerSpeechStartSentence(_ SpeakSentence: Int) {
        self.startClosure?(view)
    }

    func synthesizerSpeechEndSentence(_ SpeakSentence: Int) {
        self.endClosure?(view)
    }

    func synthesizerResumed() {

    }

    func synthesizerdidPause() {
        self.endClosure?(view)
    }

    func synthesizerCanceled() {
        self.endClosure?(view)
    }

    func synthesizerNewDataArrived(_ newData: Data!, dataFormat fmt: BDSAudioFormat, characterCount newLength: Int32, sentenceNumber SynthesizeSentence: Int) {

    }

    func synthesizerErrorOccurred(_ error: Error!, speaking SpeakSentence: Int, synthesizing SynthesizeSentence: Int) {

    }

    func synthesizerTextSpeakLengthChanged(_ newLength: Int32, sentenceNumber SpeakSentence: Int) {

    }
}
