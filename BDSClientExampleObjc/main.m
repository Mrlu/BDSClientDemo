//
//  main.m
//  BDSClientExampleObjc
//
//  Created by Mrlu on 2018/9/14.
//  Copyright © 2018 Mrlu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
