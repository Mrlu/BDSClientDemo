//
//  AppDelegate.h
//  BDSClientExampleObjc
//
//  Created by Mrlu on 2018/9/14.
//  Copyright © 2018 Mrlu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

