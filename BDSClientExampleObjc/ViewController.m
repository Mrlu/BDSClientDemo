//
//  ViewController.m
//  BDSClientExampleObjc
//
//  Created by Mrlu on 2018/9/14.
//  Copyright © 2018 Mrlu. All rights reserved.
//

#import "ViewController.h"

//#import <BDSClient/BDSClient.h>
//#import <BDSClientHelper/BDSClient.h>
//#import <BDSClientHelper/
//#import <Weibo_SDK/WeiboSDK.h>
//#import <BDSClientHelper/BDSSpeechSynthesizer.h>
//#import <BDSClientHelper/BDS.h>
//#import <BDSClientHelper/BDSSpeechSynthesizer.h>
#import <BDSClientHelper/BDS.h>
#import <BDSClientHelper/BDSSpeechSynthesizer.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [[BDS alloc] init];
    [BDSSpeechSynthesizer sharedInstance];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
